//variables declaration and initialize
var socket = io(),
    teamName = null;

//listen for events from server socket

//users list event
socket.on('whosin', function(data){
    var tnames = JSON.parse(data)
    $( '#whosin' ).empty();
    //adds team name into users div list
    tnames.forEach(function(name){
        $('#whosin').append('<li><p class="text-dark">'+name.tname+'</p></li>');
    })  
});

//receive question event
socket.on('receiveQuestion', function(no, text){
    document.getElementById("questionNo").innerHTML = "Soalan No. "+no
    document.getElementById("questionText").innerHTML = text
    document.getElementById("btnAns").classList.add('btn-primary');
    document.getElementById("btnAns").disabled=false
})

//clear display event
socket.on('ClearDisplay',function(){
                console.log("clear")
                document.getElementById("Question").innerHTML = " ";
                document.getElementById("Choice").innerHTML = " ";
                document.getElementById("btnAns").classList.remove('btn-primary');
                document.getElementById("btnAns").disabled=true
            });


socket.on("ClientQuestion", function(data){
    if(TeamName != null){
        $("#fastestTeam").empty();
        var obj = JSON.parse(data);
        var quest = obj.Question
        var choice = obj.Answer
        var image = obj.Image

        if (quest.length > 100){
            //document.getElementById('QuestionLayout').className = "col-md-9"
        }else if (quest.length > 10 && quest.length < 30){
            //document.getElementById('QuestionLayout').className = "col"
        }

        if (image == null){
            document.getElementById("image-div").style.display = "none";
            //QuestionImage.src = "assets/image/dummy_no_image.png";
        }else{
            document.getElementById("image-div").style.display = "block";
            QuestionImage.src = "/upload/" + image;
        }

        document.getElementById('Question').innerHTML = quest;
        document.getElementById('Choice').innerHTML = choice;
        document.getElementById("btnAns").disabled = false;
        document.getElementById("btnAns").classList.add('btn-primary');
        
    }
});

//functions

//input team name function
function inputTeamName(){
    TeamName = prompt("Enter Team Name :");
    if (TeamName == "" || TeamName == null || TeamName == undefined){
        alert("Team Name can't be empty!");
        inputTeamName();
    }else{
		document.getElementById("teamName").innerHTML = TeamName;
        socket.emit('TeamName', TeamName);
    }
}

//buzzer function
function Buzzer(){
    document.getElementById("teamName").className = "FadeIn"
    socket.emit('Client_Answer', TeamName);
	document.getElementById("btnAns").classList.remove('btn-primary');
	document.getElementById("btnAns").disabled=true
    
}