//variables declaration and initialize
var socket = io(),
    width = window.screen.width * window.devicePixelRatio,
    height = window.screen.height * window.devicePixelRatio,
    teamNum=1,
    displayTime = document.getElementById('timer'),
    times=10,
    minutes,
    seconds,
    timerInterval,
    videoInterval;

console.log(width)
console.log(height)

//jqeury ready function
$(document).ready(function () {
    var teamNum=1;
    //fastest team event
    socket.on('FastestTeam',function(obj){
        console.log(obj)
        if(teamNum==1){
            displaylogo(obj.TeamName)
        }
        if(teamNum<4){
            $("#fastestTeam").append('<li>' + "[" + obj.TeamAnswerTime + "] - " + obj.TeamName + '</li>');
        console.log(teamNum)
        }
        teamNum++;
    })
    
    //receive question event
    socket.on("ClientQuestion", function(data){
        teamNum=1
        console.log(teamNum)
        $("#fastestTeam").empty();
        var obj = JSON.parse(data);
        var quest = obj.Question
        var choice = obj.Answer
        var image = obj.Image

        if (quest.length > 100){
            document.getElementById('QuestionLayout').className = "col-md-9"
        }else if (quest.length > 10 && quest.length < 30){
            document.getElementById('QuestionLayout').className = "col"
        }

        if (image == null){
            document.getElementById("image-div").style.display = "none";
            //QuestionImage.src = "assets/image/dummy_no_image.png";
        }else{
            document.getElementById("image-div").style.display = "block";
            QuestionImage.src = "/upload/" + image;
        }
        document.getElementById('Question').innerHTML = quest;
        document.getElementById('Choice').innerHTML = choice;
        document.getElementById("display1").style.display = "none";
        document.getElementById("display0").style.display = "block";
    });
});    
        
//listen for events from server socket

//display scores event        
socket.on("DisplayScore", function(data){
    if (data != 0 || data != "0"){
        var obj = JSON.parse(data);

        for(var i = 0; i < 8; i++){
            var index = (i + 1);
            document.getElementById("team" + index).innerHTML = "<label>" + obj[i].TEAMNAME + "</label><hr />" + obj[i].TEAMSCORE;
        }
        
        document.getElementById("display0").style.display = "none";
        document.getElementById("display1").style.display = "block";
    }else{
        document.getElementById("display1").style.display = "none";
        document.getElementById("display0").style.display = "block";
    }
});

//clear display event
socket.on('ClearDisplay',function(){
    document.getElementById("display1").style.display = "none";
    document.getElementById("display0").style.display = "none";
    document.getElementById("bg").style.backgroundImage = 'url("assets/image/tbt/Picture3.png")'
});

//timer event
socket.on("timer", function(data){
    var timeOpt=data;
    if (timeOpt=="start1") {
        timerStart(1);
    }
    else if (timeOpt=="start2") {
        timerStart(2)
    }
    else if (timeOpt=="start3") {
        timerStart(3)
    }
    else if (timeOpt=="stop") {
        timerStop()
    }
    else if (timeOpt=="reset") {
        timerReset()
    }
})

//change background color based on answer correct or wrong event
socket.on("AnswerResult_", function(data){
    if(data){
        document.getElementById("QuestionCard").style.backgroundColor = "rgba(76,175,80,0.9)";
    }else{
        document.getElementById("QuestionCard").style.backgroundColor = "rgba(213,0,0,0.9)";
    }
    setTimeout(function(){
        document.getElementById("QuestionCard").style.backgroundColor = "rgba(255,255,255,0.9)";
    }, 2000)
    
})

//video event
socket.on("video", function(index){
    videoControl(index);
})

//fastest team event
socket.on('FastestTeam',function(obj){
    console.log(obj)
    if(teamNum==1){
        displaylogo(obj.TeamName)
    }
    if(teamNum<4){
        $("#fastestTeam").append('<li>' + "[" + obj.TeamAnswerTime + "] - " + obj.TeamName + '</li>');
    console.log(teamNum)
    }
    teamNum++;
})

//receive question event
socket.on("ClientQuestion", function(data){
    teamNum=1
    console.log(teamNum)
    $("#fastestTeam").empty();
    var obj = JSON.parse(data);
    var quest = obj.Question
    var choice = obj.Answer
    var image = obj.Image

    if (quest.length > 100){
        document.getElementById('QuestionLayout').className = "col-md-9"
    }else if (quest.length > 10 && quest.length < 30){
        document.getElementById('QuestionLayout').className = "col"
    }

    if (image == null){
        document.getElementById("image-div").style.display = "none";
        //QuestionImage.src = "assets/image/dummy_no_image.png";
    }else{
        document.getElementById("image-div").style.display = "block";
        QuestionImage.src = "/upload/" + image;
    }
    document.getElementById('Question').innerHTML = quest;
    document.getElementById('Choice').innerHTML = choice;
    document.getElementById("display1").style.display = "none";
    document.getElementById("display0").style.display = "block";
});

//functions

//start timer function
function timerStart(min){
    console.log("timer start...")
    times=min*60
    timerInterval = setInterval(function () {
            
        
        minutes = parseInt(times / 60, 10)
        seconds = parseInt(times % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        displayTime.innerHTML = minutes + ":" + seconds;
        if (--times < 0) {
                clearInterval(timerInterval);
        }     
    }, 1000);    
}

//stop timer function
function timerStop(){
    console.log("timer stop...");
    clearInterval(timerInterval);
}

//reset timer function
function timerReset(){
    console.log("timer reset...");
    times=0;
    minutes = parseInt(times / 60, 10)
    seconds = parseInt(times % 60, 10);

    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
    displayTime.innerHTML = minutes + ":" + seconds;
    clearInterval(timerInterval);
}

//display team logo function
function displaylogo(team){
    document.getElementById("display0").style.display = "none";
    document.getElementById("display2").style.display = "block";
    console.log(team)
    if (team=="APM") {
        $('#logoAPM').fadeIn();
        $('#logoAPM').delay(5000).fadeOut();
    }
    else if (team=="MAC") {
        $('#logoMAC').fadeIn();
        $('#logoMAC').delay(5000).fadeOut();
    }else if (team=="GPMS") {
        $('#logoGPMS').fadeIn();
        $('#logoGPMS').delay(5000).fadeOut();
    }else if (team=="OBEY 2") {
        $('#logoOBEY').fadeIn();
        $('#logoOBEY').delay(5000).fadeOut();
    }else if (team=="OBEY 3") {
        $('#logoOBEY').fadeIn();
        $('#logoOBEY').delay(5000).fadeOut();
    }else if (team=="SMPP") {
        $('#logoSMPP').fadeIn();
        $('#logoSMPP').delay(5000).fadeOut();
    }else if (team=="ELITE GIRLS") {
        $('#logoEG').fadeIn();
        $('#logoEG').delay(5000).fadeOut();
    }else if (team=="PROSTAR") {
        $('#logoPROSTAR').fadeIn();
        $('#logoPROSTAR').delay(5000).fadeOut();
    }
    setTimeout(function(){
        document.getElementById("display0").style.display = "block";
        document.getElementById("display2").style.display = "none";
    }, 6000)
}

//video display control
function videoControl(i){
    console.log(i);
    if (i==4) {
        var j=i+1;
        console.log(j)
        document.getElementById("display0").style.display = "none";
        document.getElementById("display1").style.display = "none";

        document.getElementById("videoDiv"+i).style.display = "block";
        var myVid= document.getElementById("vid"+i);
        myVid.play();
        setTimeout(function(){
            hide(i)
            secVideo()
        },myVid.duration*1000)
        
        //display second video function
        function secVideo(){
            console.log("j : "+j)
            document.getElementById("videoDiv"+j).style.display = "block";
            var myVid2= document.getElementById("vid"+j);
            myVid2.play();
            videoInterval2=setInterval(function(){
                if (myVid2.ended) {
                    clearInterval(videoInterval2)
                    hide(j)
                }
            },1000)   
        }
    }
    else{
        document.getElementById("display0").style.display = "none";
        document.getElementById("videoDiv"+i).style.display = "block";
        var myVid= document.getElementById("vid"+i);
        myVid.play();
        videoInterval=setInterval(function(){
            if (myVid.ended) {
                clearInterval(videoInterval);
                hide(i);
            }
        }, 1000)
    }

    //hide video function
    function hide(ind){
        setTimeout(function(){
            if (ind==4) {
                document.getElementById("videoDiv"+ind).style.display = "none";
            }
            else{
                document.getElementById("videoDiv"+ind).style.display = "none";
            document.getElementById("display0").style.display = "block";
            }
        },2000)        
    }
}