var express = require("express");   //use express module
const fileUpload = require('express-fileupload');   //use express file upload module
var app = express();
var http = require('http').Server(app); //

//var io = require('socket.io')(http);  - Issue with latest Socket io
//https://github.com/socketio/socket.io/issues/3179
const io = require('socket.io')(http, { wsEngine: 'ws' }); //use socket.io module

const sqlite3 = require('sqlite3').verbose();              //use sqlite3 module
var fs = require('fs');                                    //use file system module

app.use(fileUpload());                                     //default option for file upload


var db = dbConnect();                                      //connect to db

//variables declaration and initialize
var TeamScore = JSON.parse('[]'),
    QNASheet = JSON.parse('[]'),
    tnames = JSON.parse('[]'),
    InitialTimeStamp = 0,
    TeamLog = JSON.parse('[]'),
    TimeStampClient = 0,
    QuestNo = 0;

//load the db
LoadDatabase();

app.use(express.static(__dirname + '/public')); //serve static page
app.get('/', function(req, res){
    res.sendFile(__dirname + '/public/');   
});

app.post('/upload', function(req, res) {    //post file uploaded
    var filename;
    if (typeof req.files.sampleFile != 'undefined'){
        filename = guid() + ".jpg";
        
        let sampleFile = req.files.sampleFile;
 
        sampleFile.mv(__dirname + '/public/upload/' + filename, function(err) {
            if (err) {
                return res.status(500).send(err);
            }
        });
    }else{
        filename = null;
    }
    
    //execute query one by one
    db.serialize(function(){
        var stmt = db.prepare("INSERT INTO QNA (QNA_QUESTION, QNA_ANSWER, QNA_CORRECT_ANSWER, QNA_DIR_IMAGE, QNA_DIR_OTHER) VALUES (?, ?, ?, ?, ?);")
        stmt.run(req.body.question, req.body.answers,req.body.CorrectAnswer, filename, req.body.QuestionType);
        stmt.finalize();
    });
    
    if (!req.files){    //if no file uploaded
        return res.status(400).send('No files were uploaded.');
    }
    
    
    //res.writeHead(301, {Location: 'http://localhost:3000/'});
    res.write('<script>alert("Upload Succesfully"); window.location.replace("http://localhost:3000/Admin.html") </script>');
    res.end();
    
    console.log("Added Question Succesfully!");
    dbReload();
});


var path = process.argv[2];
fs.readdir("./public/assets/image/", function(err, items) {
    ImgItems = items;
});

/*
 *Socket IO [.on] Section
*/
io.on('connection', function(socket){   //when client socket trying to connect to server socket
    var id = socket.id
    tnames.push({"id" : id, "tname" : null});   //add elements into tnames[]
    console.log(id + " Connected");
    

//listen for events from client socket

    //when a socket disconnect from server
    socket.on('disconnect', function(){
        console.log(id + " Disonnected");
        tnames.forEach(function(item, index, obj){
            if(item.id == id){
                obj.splice(index, 1); //remove element from array
            }
            
            if(item.tname == null){
                obj.splice(index, 1); //remove element from array
            }
        })
        WhosOn9();
    });
    
    socket.on("AnswerResult", function(data){
        io.emit("AnswerResult_", data);
    })

    //receive team name from client.html
    socket.on('TeamName', function(data){
        tnames.forEach(function(obj){
            if(obj.id == id){
                obj.tname = data;
            }
        })
        WhosOn9();
    });
    
    //Add Question
    socket.on('AddQuestion',function(data){
        addQuestion(data);
    });
    socket.on('Server_loadData',function(){
        emitData2Admin()
    });

    socket.on('sendQuestion', function(qNo, qText){
        io.emit('receiveQuestion', qNo, qText)
    })
        
    socket.on('UpdateTeamScore', function(data){
        setTeamScore(data);
    });
    
    socket.on('Server_ReloadData',function(){
        LoadDatabase();
        setTimeout(function(){
            emitData2Admin();   
        }, 2000);
        console.log("Reload Request Recived")
    });
    
    socket.on('Server_ResetDatabase',function(){
        resetDatabase();
        setTimeout(function(){
            LoadDatabase();
        });
        console.log("Reset Database Request Recived")
    });
    
    socket.on("Server_DisplayScore", function(flag_value){
        console.log(flag_value)
        if (flag_value == 1){
            io.emit("DisplayScore", JSON.stringify(TeamScore));
        }else{
            io.emit("DisplayScore", 0);
        }
    });
    
    socket.on('ClearDisplay', function(){
       io.emit('ClearDisplay'); 
    });
    
    socket.on("QuestIndex", function(QuestIndex){
        console.log("Send Question to Client")
        console.log(QuestIndex)
        QuestNo = QuestIndex + 1;
        io.emit('ClientQuestion', JSON.stringify(QNASheet[QuestIndex]))
        InitialTimeStamp = Date.now();
        console.log(QNASheet[QuestIndex])
    })

    socket.on("timer", function(data){
        io.emit("timer", data);
    })
    
    socket.on("video", function(index){
        io.emit("video", index);
    })
    //TODO : SAVE CLIENT ANSWER TIME
    var TeamAnswerTime;
    socket.on('Client_Answer', function(TeamName){
        TimeStampClient = Date.now();
        io.emit('Server_TeamAnswer', TeamName);
        TeamAnswerTime = ((TimeStampClient - InitialTimeStamp) + " ms");
        var obj = {"QuestionNO" : QuestNo, "TeamName" : TeamName, "TeamAnswerTime" : TeamAnswerTime};
        TeamLog.push(obj)
        console.log(obj);
        
        io.emit("FastestTeam", obj);
    })
    
    socket.on("Request_TimeReport", function(){
        io.emit("TimeReport", JSON.stringify(TeamLog));
    })
});


//functions 

function WhosOn9(){
        tnames.forEach(function(item, index, obj){
            if(item.tname == null){
                obj.splice(index, 1);
            }
        })
        io.emit("whosin", JSON.stringify(tnames))
    }

function emitData2Admin(){
    io.emit('QNASheet', JSON.stringify(QNASheet));
    io.emit('Admin_TeamScoreData', JSON.stringify(TeamScore))
}

/*
 *  Database Functions
*/
function dbConnect(){
    //https://github.com/mapbox/node-sqlite3/issues/674
    //var conn = new sqlite3.Database("db/tbt.db", sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, function (err) {
    //var conn = new sqlite3.Database("db/tbt_test2.db", sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, function (err) {
    var conn = new sqlite3.Database("db/tbt_official.db", sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, function (err) {
    //var conn = new sqlite3.Database("db/tbt_official - Copy.db", sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, function (err) {
        if (err) console.log(err.message);
    });
    return conn;
}

function LoadDatabase(){
    db = dbConnect();

    TeamScore.length = 0;
    QNASheet.length = 0;
    
    db.each("SELECT * FROM QNA", function(err, row){
        if (typeof row.QNA_ID != 'undefined'){
            QNASheet.push({QID : row.QNA_ID, Question : row.QNA_QUESTION, Answer : row.QNA_ANSWER, CorrectAnswer : row.QNA_CORRECT_ANSWER, Image : row.QNA_DIR_IMAGE, Type : row.QNA_DIR_OTHER})
        }
    });
    
    db.each("SELECT * FROM TEAM", function(err, row){
        TeamScore.push({TEAMID : row.TEAM_ID, TEAMNAME : row.TEAM_NAME, TEAMSCORE : row.TEAM_SCORE});
    });

    console.log("Loaded Data Succesfully")
}

function dbReload(){
    setTimeout(function(){
        LoadDatabase()
    }, 3000)
}

function setTeamScore(data){
    data = JSON.parse(data);
    var stmt = db.prepare("UPDATE TEAM SET TEAM_NAME = ?, TEAM_SCORE = ? WHERE TEAM_ID = ?;");
    data.forEach(function(obj){
        stmt.run(obj.TeamName, obj.TeamScore, obj.TeamID);
    });
    stmt.finalize();
    console.log("Succesfully Updated team score!")
    dbReload();
}
function addQuestion(data){
    var obj = JSON.parse(data);
    db.serialize(function(){
        var stmt = db.prepare("INSERT INTO QNA (QNA_QUESTION, QNA_ANSWER, QNA_CORRECT_ANSWER) VALUES (?, ?, ?);")
        stmt.run(obj.Question, obj.Answer,obj.CorrectAnswer);
        stmt.finalize();
    });
    console.log("Added Question Succesfully!");
    dbReload();
}
function resetDatabase(){
    db.run("DELETE FROM QNA");
    db.run("delete from sqlite_sequence where name='QNA'")
    
    var stmt = db.prepare("UPDATE TEAM SET TEAM_NAME = ?, TEAM_SCORE = ? WHERE TEAM_ID = ?;");
    for (var i = 0; i < 10; i++){
        var index = i + 1;
        stmt.run("Team" + index, "0", index);
    }
    stmt.finalize();
    dbReload();
}

function WriteTeamScoreLog(){
    var fileTeamLog = guid();
    fs.writeFile(__dirname + "/logs/" + fileTeamLog + ".txt" , JSON.stringify(TeamLog), function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    });
}

//Generate unique id
//https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript?page=1&tab=votes#tab-top
function guid() {
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function s4() {
  return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
}

http.listen(3000, function(){
  console.log('listening on *:3000'); //set the port to listen for connection
});